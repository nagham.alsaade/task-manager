<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;

class RegisterRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:users,name|max:255|min:1',
            'email' => 'required|email|unique:users,email|max:255|min:1|regex:/(.+)@(.+)\.(.+)/i',
            'password' => 'required|string|max:255|min:8|confirmed',
        ];
    }
}
