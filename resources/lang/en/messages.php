<?php

    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    */

return [

    'updated_successfully' => 'Updated Successfully',
    'added_successfully' => 'Added Successfully',
    'deleted_successfully' => 'Deleted Successfully',
    'get_data_successfully' => 'Get Data Successfully',
];
