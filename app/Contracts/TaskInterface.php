<?php

namespace app\Contracts;

use App\Contracts\Eloquent\EloquentInterface;

interface TaskInterface extends EloquentInterface
{
    public function changeStatus(int $task_id, bool $new_status): bool;
    public function assignmentMember(int $task_id, int $user_id): bool;
    public function joinTask(int $task_id): bool;
    public function leaveTask(int $task_id): bool;
} 