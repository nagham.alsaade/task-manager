<?php

namespace App\Http\Resources\Log;

use App\Models\Project;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ChangesLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $user_name = User::find($this->resource->user_id)->name;

        $data = [

            'id' =>  $this->resource->id,
            'description' =>  $this->resource->description,
            'user_id' =>  $this->resource->user_id,
            'user_name' =>  $user_name,
            'model_type' =>  $this->resource->description,
            'model_id' =>  $this->resource->model_id,
            'created_at' => Carbon::parse($this->resource->created_at)->format('M d, Y h:i A'),
        ];

        if($this->resource->model_type == Project::class) {
            $data['model_type'] =  'Project';

        } else {
            $data['model_type'] = 'Task';
        }

        return $data;
    }
}
