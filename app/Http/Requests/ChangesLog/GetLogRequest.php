<?php

namespace App\Http\Requests\ChangesLog;

use App\Http\Requests\BaseRequest;

class GetLogRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules =  [
            'model_type' => 'required|string|in:Project,Task',
            'model_id' => 'required|integer',
            'per_page' => 'nullable|integer',
        ];

        if ($this->input('model_type') == 'Project') {
            $rules['model_id'] .= '|exists:projects,id';
        } else {
            $rules['model_id'] .= '|exists:tasks,id';
        }

        return $rules;
    }
}
