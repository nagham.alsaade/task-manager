<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'verify_code' => 'Please verify code to complete login.',
    'register_success' => 'Your account has been created successfully.',
    'wrong_credentials' => 'These credentials do not match our records, please try again.',
    'login_success' => 'You have been logged in successfully.',
    'logout_success' => 'You have successfully logged out.',
    'success_verified' => 'The code has been confirmed successfully.',
    'error_code' => 'The code you entered is incorrect, please try again and enter the code you received in your email.',
    'reset_password_success' => 'A new password has been set successfully.',
    'wrong_old_password' => 'Old Password do not match your password, please try again.',
    'change_password_success' => 'The password has been changed successfully.',
    'user_not_verified' => 'This account is not verified.',
    'code_sent_success' => 'The code has been sent successfully.',
    'error_send_code' => 'There was a problem sending the code, please try again.',
];
