<?php

namespace App\Services;
use App\Models\ChangesLog;

/**
 * Class ChangesLogService.
 */
class ChangesLogService
{

    public static function store($user_id, $model_type, $model_id, $description)
    {
        ChangesLog::create([
            'description'  => $description,
            'model_type' => $model_type,
            'model_id' => $model_id,
            'user_id' => $user_id,
        ]);
        return true;
    }
}
