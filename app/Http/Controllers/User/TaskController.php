<?php

namespace App\Http\Controllers\User;

use App\Contracts\TaskInterface;
use App\Enums\ResponseEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Task\AssignmentRequest;
use App\Http\Requests\Task\ChangeStatusRequest;
use App\Http\Requests\Task\CreateRequest;
use App\Http\Requests\Task\GetRequest;
use App\Http\Requests\Task\JoinRequest;
use App\Http\Requests\Task\LeaveRequest;
use App\Http\Requests\Task\UpdateRequest;

class TaskController extends Controller
{
    public function __construct(private TaskInterface $taskInterface)
    {
    }

    public function create(CreateRequest $request)
    {
        $request = array_filter($request->validated(), function ($value) {
            return ($value !== null);
        });

        $success = $this->taskInterface->create($request);
        return $this->sendResponse(ResponseEnum::ADD, $success);
    }

    public function show(GetRequest $request)
    {
        $success = $this->taskInterface->findById($request->task_id);
        return $this->sendResponse(ResponseEnum::GET, $success);
    }

    public function update(UpdateRequest $request)
    {
        $request = array_filter($request->validated(), function ($value) {
            return ($value !== null);
        });

        $success = $this->taskInterface->update($request['task_id'], $request);
        return $this->sendResponse(ResponseEnum::UPDATE, $success);
    }

    public function delete(GetRequest $request)
    {
        $success = $this->taskInterface->delete($request->task_id);
        return $this->sendResponse(ResponseEnum::DELETE, $success);
    }

    public function changeStatus(ChangeStatusRequest $request)
    {
        $success = $this->taskInterface->changeStatus($request->task_id, $request->new_status);
        return $this->sendResponse(ResponseEnum::UPDATE, $success);
    }

    public function joinTask(JoinRequest $request)
    {
        $success = $this->taskInterface->joinTask($request->task_id);
        return $this->sendResponse(ResponseEnum::ADD, $success);
    }

    public function leaveTask(LeaveRequest $request)
    {
        $success = $this->taskInterface->leaveTask($request->task_id);

        if(!$success) {
            return $this->sendError(__('errors.already_join_task'));
        }

        return $this->sendResponse(ResponseEnum::DELETE, $success);
    }

    public function assignmentMember(AssignmentRequest $request)
    {
        $success = $this->taskInterface->assignmentMember($request->task_id, $request->user_id);

        if(!$success) {
            return $this->sendError(__('errors.already_join_task'));
        }

        return $this->sendResponse(ResponseEnum::ADD, $success);
    }
}
