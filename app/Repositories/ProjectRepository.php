<?php

namespace app\Repositories;

use App\Contracts\ProjectInterface;
use App\Enums\RoleTypeEnum;
use App\Http\Resources\Project\ProjectDetailsResource;
use App\Http\Resources\Project\ProjectResource;
use App\Jobs\ProcessLogJob;
use App\Models\Project;
use App\Models\User;
use App\Models\UserProject;
use App\Services\ImageService;
use App\Services\ProjectService;

class ProjectRepository implements ProjectInterface
{
    public function create($payload)
    {
        if (isset($payload['background_image'])) {
            $payload['background_image'] = ImageService::uploadFile($payload['background_image'], 'projects_background');
        }
        $user = User::find(auth()->user()->id);
        $project = Project::create($payload);
        $user->projects()->attach($project->id, ['role' => RoleTypeEnum::Administrator]);

        $description = $user->name . " create " . $project->title . " project.";
        dispatch(new ProcessLogJob($user->id, Project::class, $project->id, $description));

        return $project;
    }

    public function update($project_id, $payload): bool
    {
        cache()->forget('project.details'); // Clear cached

        $user = auth()->user();

        $project = Project::find($project_id);

        if (isset($payload['background_image'])) {
            if ($project->background_image === "files/projects_background/default.jpg") {
                $payload['background_image'] = ImageService::uploadFile($payload['background_image'], 'projects_background');
            } else {
                $payload['background_image'] = ImageService::updateFile($payload['background_image'], $project->background_image, 'projects_background');
            }
        }

        $project->update($payload);

        $description = $user->name . " update " . $project->title . " project.";
        dispatch(new ProcessLogJob($user->id, Project::class, $project->id, $description));

        return true;
    }

    public function delete($project_id): bool
    {
        cache()->forget('project.details'); // Clear cached

        $user = auth()->user();

        $project = Project::find($project_id);

        if ($project->background_image != "files/projects_background/default.jpg") {
            ImageService::deleteFile($project->background_image);
        }

        $description = $user->name . " delete " . $project->title . " project.";
        dispatch(new ProcessLogJob($user->id, Project::class, $project_id, $description));

        $project->delete();
        return true;
    }

    public function findById($project_id)
    {
        // Cache Task lists, changes, and users in project
        $project = cache()->remember('project.details', now()->addMinutes(10), function () use ($project_id) {
            return Project::where('id', $project_id)->with('users', 'tasks')->first();
        });

        return new ProjectDetailsResource($project);
    }

    public function changeStatus($project_id, $new_status): bool
    {
        cache()->forget('project.details'); // Clear cached

        $user = auth()->user();

        $project = Project::find($project_id);
        $project->update(['status' => $new_status]);

        $description = $user->name . " change " . $project->title . " project status.";
        dispatch(new ProcessLogJob($user->id, Project::class, $project->id, $description));

        return true;
    }

    public function addMember($project_id, $user_id): bool
    {
        cache()->forget('project.details'); // Clear cached

        $user = User::find(auth()->user()->id);

        $already_joined = ProjectService::checkIfUserJoined($project_id, $user_id);
        if ($already_joined) {
            return false;
        }

        $new_user = USer::find($user_id);

        $new_user->projects()->attach($project_id);

        $description = $user->name . " add " . $new_user->name . " on project.";
        dispatch(new ProcessLogJob($user->id, Project::class, $project_id, $description));

        return true;
    }

    public function changeMemberRole($project_id, $user_id, $new_role): bool
    {
        cache()->forget('project.details'); // Clear cached

        $user = auth()->user();

        $user_project = UserProject::where([['user_id', $user_id], ['project_id', $project_id]])->first();

        $user_project->update(['role' => $new_role]);

        $changed_user = User::find($user_id);

        $description = $user->name . " change role for user" . $changed_user->name . " on project.";
        dispatch(new ProcessLogJob($user->id, Project::class, $project_id, $description));

        return true;
    }

    public function all($search, $per_page)
    {
        $user = auth()->user();
        $user_project_ids = UserProject::where('user_id', $user->id)->select('project_id');
        $projects = Project::whereIn('id', $user_project_ids)->with('users');

        if (isset($search)) {
            $projects->where('title', 'LIKE', '%' . $search . '%')->orWhere('description', 'LIKE', '%' . $search . '%');
        }

        return ProjectResource::collection($projects->get());
    }
}
