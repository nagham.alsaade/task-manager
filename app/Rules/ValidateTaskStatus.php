<?php

namespace App\Rules;

use App\Enums\ProjectStatusEnum;
use App\Enums\TaskStatusEnum;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class ValidateTaskStatus implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if(!TaskStatusEnum::isValidValue($value)) {
            $fail('The :attribute must be a valid task status.');
        };
    }
}
