<?php

namespace app\Repositories\Eloquent;

use App\Contracts\Eloquent\EloquentInterface;
use Illuminate\Database\Eloquent\Collection;

use Illuminate\Database\Eloquent\Model;

class BaseRepository implements EloquentInterface
{
    protected $model;

    public function __construct(Model $model) {
        $this->model = $model;
    }

    public function findById(int $modelId): ?Model
    {
        return null;
    }

    public function all(?string $search, ?int $per_page): ?Collection 
    {
        return null;
    }

    public function create(array $payload): ?Model
    {
        return null;
    }

    public function update(int $modelId, array $payload): bool
    {
        return true;
    }
    
    public function delete(int $modelId): bool
    {
        return true;
    }
}