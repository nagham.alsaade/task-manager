<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class ResponseEnum extends Enum
{
    const GET = ('message.get_data_successfully');
    const ADD = ('message.added_successfully');
    const DELETE = ('message.deleted_successfully');
    const UPDATE = ('message.updated_successfully');
}
