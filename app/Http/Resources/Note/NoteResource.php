<?php

namespace App\Http\Resources\Note;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class NoteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $user_name = User::find($this->resource->user_id)->name;
        
        return [
            'id' =>  $this->resource->id,
            'description' =>  $this->resource->description,
            'user_id' =>  $this->resource->user_id,
            'user_name' =>  $user_name,
            'created_at' => Carbon::parse($this->resource->created_at)->format('M d, Y h:i A'),
            'updated_at' => Carbon::parse($this->resource->created_at)->format('M d, Y h:i A'),
        ];
    }
}
