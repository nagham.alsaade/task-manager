<?php

namespace app\Contracts;


interface ChangesLogInterface
{
    public function show(int $model_type, int $model_id, $per_page);
} 