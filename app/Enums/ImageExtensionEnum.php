<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static JPEG()
 * @method static static BMP()
 * @method static static SVG()
 * @method static static PNG()
 * @method static static JPG()
 * @method static static GIF()
 */
final class ImageExtensionEnum extends Enum
{
    const JPEG = "jpeg";
    const BMP = "bmp";
    const SVG = "svg";
    const PNG = "png";
    const JPG = "jpg";
    const GIF = "gif";
}
