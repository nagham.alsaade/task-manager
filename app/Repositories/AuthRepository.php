<?php

namespace app\Repositories;

use App\Contracts\AuthInterface;
use App\Models\User;
use App\Services\EmailService;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthRepository implements AuthInterface
{

    public function register($name, $email, $password): bool
    {
        // Create random verified code.
        $code = mt_rand(100000, 999999);

        try {
            // Send Code To User's Email Address.
            $result = EmailService::sendCode($email, $code);
        } catch (Exception $e) {
            return $result;
        }

        // Create uer account.
        User::create([
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'verified_code' => $code,
            'code_expires_at' => now()->addMinutes(10),
        ]);

        return true;
    }

    public function confirmAccount($email, $guard, $code)
    {
        $user = User::where('email', $email)->first();

        // Check if code is correct and is still valid.
        if ($user->verified_code == $code && now()->lt($user->code_expires_at)) {
            $token = $user->createToken('MyApp', [$guard])->plainTextToken;
            if (!$token) {
                return false;
            }

            $user->email_verified_at = now();
            $user->is_verified = true;
            $user->save();

            $data['token'] = $token;

            return $data;
        }
        return false;
    }

    public function login($email, $password, $guard)
    {
        $data = [];

        $credentials = ['email' => $email, 'password' => $password];

        if (Auth::guard($guard)->attempt($credentials)) {
            $user = Auth::guard($guard)->user();

            // Create token.
            $token = $user->createToken('MyApp', [$guard])->plainTextToken;

            if (!$token) {
                return false;
            }
            $data['id'] = $user->id;
            $data['token'] = $token;

            return $data;
        }
        return false;
    }

    public function logout($payload): bool
    {
        // Get user who requested the logout.
        $user = request()->user();

        // Revoke current user token.
        $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();

        return true;
    }

    public function changePassword($account, $old_password, $new_password): bool
    {
        // Check if old password is correct.
        if (!Hash::check($old_password, $account->password)) {
            return false;
        } else {
            $account->update(['password' => $new_password]);
        }
        return true;
    }

    public function forgetPassword($account): bool
    {
        // Create new random Verified Code.
        $code = mt_rand(100000, 999999);

        try {
            // Send Code To User's Email Address.
            $result = EmailService::sendCode($account->email, $code);
        } catch (Exception $e) {
            return $result;
        }

        $account->update(['verified_code' => $code, 'code_expires_at' => now()->addMinutes(10)]);

        return true;
    }

    public function resendCode($account): bool
    {
        // Create new random Verified Code.
        $code = mt_rand(100000, 999999);

        try {
            // Send Code To User's Email Address.
            $result = EmailService::sendCode($account->email, $code);
        } catch (Exception $e) {
            return $result;
        }

        $account->update(['verified_code' => $code, 'code_expires_at' => now()->addMinutes(10)]);

        return true;
    }

    public function confirmCode($account, $code): bool
    {
        // Check if code is correct and is still valid.
        if ($account->verified_code == $code && now()->lt($account->code_expires_at)) {
            return true;
        }
        return false;
    }

    public function resetPassword($account, $new_password): bool
    {
        $account->update(['password' => $new_password]);
        return true;
    }

    public function showProfile($account)
    {
        return $account;
    }
}
