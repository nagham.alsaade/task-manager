<?php

return [

     /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'verify_code' => 'يجب عليك تأكيد الرمز لإتمام عملية الدخول',
    'register_success' => 'تم تسجيل الدخول بنجاح.',
    'wrong_credentials' => '.هذه المدخلات لا تتطابق مع قاعدة البيانات، الرجاء المحاولة مجدداً',
    'login_success' => '.تم تسجيل الدخول بنجاح',
    'logout_success' => '.تم تسجيل الخروج بنجاح',
    'success_verified' => '.تم إرسال الرمز بنجاح',
    'error_code' => '.الرمز الذ قمت بإدخاله غير صحيح، الرجاء المحاولة من جديد وادخال الرمز الذي تلقيته على بريدك الخاص',
    'reset_password_success' => '.تم تعيين كلمة مرور جديدة بنجاح',
    'wrong_old_password' => '.كلمة المرور القديمة التي أدخلتها غير متوافقة مع كلمة المرور الخاصة بك، الرجاء المحاولة مجدداً',
    'change_password_success' => '.تم تغيير كلمة المرور بنجاح',
    'user_not_verified' => '.لم يتم تأكيد هذا الحساب',
    'code_sent_success' => '.تم إرسال الرمز',
    'error_send_code' => 'حدثت مشكلة أثناء إرسال الرمز، الرجاء المحاولة من جديد.',
];
