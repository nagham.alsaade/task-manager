<?php

namespace app\Contracts\Eloquent;

interface EloquentInterface
{
    public function create(array $payload);
    public function findById(int $model_id);
    public function all(?string $search, ?int $per_page);
    public function update(int $model_id, array $payload): bool;
    public function delete(int $model_id): bool;
}