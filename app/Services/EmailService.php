<?php

namespace App\Services;

use App\Mail\SendCodeViaEmail;
use Exception;
use Illuminate\Support\Facades\Mail;

class EmailService
{
    public static function sendCode($email, $code)
    {
        $verificationCode = [
            'title' => 'This Is Your Verification Code',
            'body' => $code,
        ];

        try {
            $result = Mail::to($email)->send(new SendCodeViaEmail($verificationCode));
        } catch (Exception $e) {
            return $result;
        }

        return true;
        // dd('Success! Email has been sent successfully.');
    }
}
