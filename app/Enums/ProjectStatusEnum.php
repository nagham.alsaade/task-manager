<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Closed()
 * @method static static Open()
 */
final class ProjectStatusEnum extends Enum
{
    const Closed = 0;
    const Open = 1;
}
