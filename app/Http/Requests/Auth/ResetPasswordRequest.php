<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;

class ResetPasswordRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users,email|max:255|min:1|regex:/(.+)@(.+)\.(.+)/i',
            'new_password' => 'required|string|max:255|min:8|confirmed',
        ];
    }
}
