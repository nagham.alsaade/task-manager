<?php

namespace app\Contracts;

use App\Contracts\Eloquent\EloquentInterface;

interface NoteInterface extends EloquentInterface
{
} 