<?php

namespace app\Repositories;

use App\Contracts\NoteInterface;
use App\Jobs\ProcessLogJob;
use App\Models\Note;
use App\Models\Task;
use App\Services\ChangesLogService;

class NoteRepository implements NoteInterface
{
    public function create($payload)
    {
        $user = auth()->user();
        $payload['user_id'] = $user->id;
        $note = Note::create($payload);

        $description = $user->name . " add new note to task.";
        dispatch(new ProcessLogJob($payload['user_id'], Task::class, $payload['task_id'], $description));

        return $note;
    }

    public function update($note_id, $payload): bool
    {
        $user = auth()->user();

        $note = Note::find($note_id);
        $note->update($payload);

        $description = $user->name . " update task.";
        dispatch(new ProcessLogJob($user->id, Task::class, $payload['task_id'], $description));

        return true;
    }

    public function delete($note_id): bool
    {
        $user = auth()->user();

        $note = Note::find($note_id);

        $description = $user->name . " delete note in this task.";
        dispatch(new ProcessLogJob($user->id, Task::class, $note->task_id, $description));

        $note->delete();

        return true;
    }

    public function all($search, $per_page)
    {
    }

    public function findById(int $note_id)
    {
    }
}
