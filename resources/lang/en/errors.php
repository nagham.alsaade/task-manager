<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error Language Lines
    |--------------------------------------------------------------------------
    */

    'user_not_administrator' =>  'This action cannot be taken, it is not within your authority.',
    'user_not_joined' => 'This action cannot be taken, you are not joined to the project.',
    'already_join_project' => 'This user already has joined to this project.',
    'already_join_task' => 'This user already has joined to this task.',
    'user_viewer' => 'Modifications cannot be made, your permissions are only to view and track tasks.',
];
