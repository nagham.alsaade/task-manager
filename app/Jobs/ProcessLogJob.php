<?php

namespace App\Jobs;

use App\Services\ChangesLogService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessLogJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user_id, $model_type, $model_id, $description;
    
    /**
     * Create a new job instance.
     */
    public function __construct($user_id, $model_type, $model_id, $description)
    {
        $this->user_id = $user_id;
        $this->model_type = $model_type;
        $this->model_id = $model_id;
        $this->description = $description;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $user_id = $this->user_id;
        $model_type = $this->model_type;
        $model_id = $this->model_id;
        $description = $this->description;
    
        ChangesLogService::store($user_id, $model_type, $model_id, $description);
    }
}
