<?php

namespace App\Http\Requests\Project;

use App\Enums\ImageExtensionEnum;
use App\Enums\ProjectStatusEnum;
use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $imageMimeTypes = ImageExtensionEnum::getValues();
        
        return [
            'title' => 'required|string|max:255|min:3',
            'description' => 'nullable|string',
            'status' => ['nullable', 'boolean', ProjectStatusEnum::class],
            'background_image' => 'nullable|image|mimes:' . implode(',', $imageMimeTypes),
        ];
    }
}
