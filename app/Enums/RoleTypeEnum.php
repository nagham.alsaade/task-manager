<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Administrator()
 * @method static static TeamMember()
 * @method static static Viewer()
 */
final class RoleTypeEnum extends Enum
{
    const Administrator = 0;
    const TeamMember = 1;
    const Viewer = 2;

    public static function isValidValue($value)
    {
        return in_array($value, self::getValues());
    }
}
