<?php

namespace App\Rules;

use App\Enums\RoleTypeEnum;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class ValidateRoleType implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if(!RoleTypeEnum::isValidValue($value)) {
            $fail('The :attribute must be a valid role type.');
        };
    }
}
