<?php

namespace App\Http\Requests\Note;

use App\Http\Requests\BaseRequest;

class GetRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'note_id' => 'required|integer|exists:notes,id',
        ];
    }
}
