<?php

namespace App\Http\Requests\Task;

use App\Enums\TaskStatusEnum;
use App\Http\Requests\BaseRequest;
use App\Rules\ValidateTaskStatus;

class CreateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'project_id' => 'required|integer|exists:projects,id',
            'name' => 'required|string|max:255|min:3',
            'description' => 'nullable|string',
            'priority' => 'nullable|integer',
            'start_date' => 'nullable|date_format:Y-m-d H:i:s',
            'end_date' => 'nullable|date_format:Y-m-d H:i:s',
            'status' => ['nullable', 'integer', new ValidateTaskStatus()],
        ];
    }
}
