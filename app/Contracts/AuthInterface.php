<?php

namespace app\Contracts;

use Illuminate\Contracts\Auth\Authenticatable;

interface AuthInterface
{
    public function register(string $name, string $email, string $password): bool;
    public function confirmAccount(string $email, string $guard, string $code);
    public function login(string $email, string $password, string $guard);
    public function logout($payload): bool;
    public function changePassword(Authenticatable $account, string $old_password, string $new_password): bool;
    public function forgetPassword(Authenticatable $account): bool;
    public function resendCode(Authenticatable $account): bool;
    public function confirmCode(Authenticatable $account, string $code): bool;
    public function resetPassword(Authenticatable $account, string $new_password): bool;
    public function showProfile(Authenticatable $account);
} 