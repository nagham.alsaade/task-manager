<?php

namespace app\Contracts;

use App\Contracts\Eloquent\EloquentInterface;

interface ProjectInterface extends EloquentInterface
{
    public function changeStatus(int $project_id, bool $new_status): bool;
    public function addMember(int $project_id, int $user_id): bool;
    public function changeMemberRole(int $project_id, int $user_id, int $new_role): bool;
} 