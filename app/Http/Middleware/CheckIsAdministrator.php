<?php

namespace App\Http\Middleware;

use App\Services\ProjectService;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckIsAdministrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, string ...$guards): Response
    {
        $is_administrator = ProjectService::checkIfUserAdministrator($request->project_id);
        if (!$is_administrator) {

            $response = [
                'success' => false,
                'data' => [],
                'message' => (__('errors.user_not_administrator')),
            ];

            return response()->json($response, 403);
        }
        return $next($request);
    }
}
