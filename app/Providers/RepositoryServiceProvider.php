<?php

namespace App\Providers;

use App\Contracts\AuthInterface;
use App\Contracts\ChangesLogInterface;
use App\Contracts\Eloquent\EloquentInterface;
use App\Contracts\NoteInterface;
use App\Contracts\ProjectInterface;
use App\Contracts\TaskInterface;
use App\Repositories\AuthRepository;
use App\Repositories\ChangesLogRepository;
use App\Repositories\Eloquent\BaseRepository;
use App\Repositories\NoteRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\TaskRepository;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentInterface::class, BaseRepository::class);
        $this->app->bind(AuthInterface::class, AuthRepository::class);
        $this->app->bind(ProjectInterface::class, ProjectRepository::class);
        $this->app->bind(TaskInterface::class, TaskRepository::class);
        $this->app->bind(NoteInterface::class, NoteRepository::class);
        $this->app->bind(ChangesLogInterface::class, ChangesLogRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
