<?php

use App\Http\Controllers\User\Auth\AuthController;
use App\Http\Controllers\User\ChangesLogController;
use App\Http\Controllers\User\NoteController;
use App\Http\Controllers\User\ProjectController;
use App\Http\Controllers\User\TaskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register User API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['middleware' => ['languageSwitcher']], function () {
    
    // Register
    Route::post('register', [AuthController::class, 'register']);

    // Confirm Account
    Route::post('confirm_account', [AuthController::class, 'confirmAccount']);

    // Login
    Route::post('login', [AuthController::class, 'login'])->middleware(['redirectIfNotVerified']);

    // Forget Password
    Route::post('forget_password', [AuthController::class, 'forgetPassword']);

    // Reset Password
    Route::patch('reset_password', [AuthController::class, 'resetPassword']);

    // Confirm Code
    Route::post('confirm_code', [AuthController::class, 'confirmCode']);

    // Resend Code
    Route::post('resend_code', [AuthController::class, 'resendCode']);

    Route::group(['middleware' => ['auth:sanctum', 'ability:user-api']], function () {

        // Logout
        Route::get('logout', [AuthController::class, 'logout']);

        // Show Profile
        Route::get('profile', [AuthController::class, 'showProfile']);

        // Change Password
        Route::patch('change_password', [AuthController::class, 'changePassword']);

        // Project Management
        Route::group(['prefix' => 'project'], function () {
            Route::post('create', [ProjectController::class, 'create']);
            Route::get('all', [ProjectController::class, 'all']);
            Route::get('/', [ProjectController::class, 'show'])->middleware(['isUserJoined']);

            Route::group(['middleware' => 'isUserAdministrator'], function () {
                Route::patch('update', [ProjectController::class, 'update']);
                Route::delete('delete', [ProjectController::class, 'delete']);
                Route::patch('change_status', [ProjectController::class, 'changeStatus']);
                Route::post('add_member', [ProjectController::class, 'addMember']);
                Route::patch('change_member_role', [ProjectController::class, 'changeMemberRole']);
            });
        });

        // Task Management
        Route::group(['prefix' => 'task'], function () {

            Route::group(['middleware' => 'userNotViewer'], function () {
                Route::post('create', [TaskController::class, 'create']);
                Route::patch('update', [TaskController::class, 'update']);
                Route::delete('delete', [TaskController::class, 'delete']);
                Route::patch('change_status', [TaskController::class, 'changeStatus']);
                Route::post('join', [TaskController::class, 'joinTask']);
                Route::post('leave', [TaskController::class, 'leaveTask']);
                Route::post('assignment', [TaskController::class, 'assignmentMember']);
    
                Route::group(['prefix' => 'note'], function () {
                    Route::post('create', [NoteController::class, 'create']);
                    Route::delete('delete', [NoteController::class, 'delete']);
                    Route::patch('update', [NoteController::class, 'Update']);
                });
            });

            Route::get('/', [TaskController::class, 'show']);
        });

        // Changes Log Management
        Route::group(['prefix' => 'log'], function () {
            Route::get('/', [ChangesLogController::class, 'show']);
        });
    });
});
