<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChangesLog extends Model
{
    use HasFactory;

    protected $table = 'changes_logs';
    
    protected $fillable = [
        'description',
        'model_type',
        'model_id',
        'user_id',
    ];

    protected $hidden = [
    ];

    protected $casts = [
    ];

    // Relationships Methods
    public function model()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
