<?php

namespace App\Http\Resources\Task;

use App\Http\Resources\Note\NoteResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' =>  $this->resource->id,
            'name' =>  $this->resource->name,
            'description' =>  $this->resource->priority,
            'priority' =>  $this->resource->status,
            'start_date' => Carbon::parse($this->resource->start_date)->format('M d, Y h:i A'),
            'end_date' => Carbon::parse($this->resource->end_date)->format('M d, Y h:i A'),
            'status' => $this->resource->status,
            'project_id' => $this->resource->project_id,
            'created_at' => Carbon::parse($this->resource->created_at)->format('M d, Y h:i A'),
            'notes' =>  NoteResource::Collection($this->resource->notes),
        ];
    }
}
