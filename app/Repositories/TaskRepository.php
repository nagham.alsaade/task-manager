<?php

namespace app\Repositories;

use App\Contracts\TaskInterface;
use App\Http\Resources\Task\TaskResource;
use App\Jobs\ProcessLogJob;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use App\Services\TaskService;

class TaskRepository implements TaskInterface
{
    public function create($payload)
    {
        $user = auth()->user();

        if (!isset($payload['priority'])) {

            $last_task = Task::where('project_id', $payload['project_id'])->orderBy('priority', 'DESC')->first();

            if (!isset($last_task)) {
                $payload['priority'] = 0;
            } else {
                $payload['priority'] = $last_task->priority + 1;
            }
        }

        $task = Task::create($payload);

        $description = $user->name . " create " . $payload['name'] . " task in project.";
        dispatch(new ProcessLogJob($user->id, Project::class, $payload['project_id'], $description));
        dispatch(new ProcessLogJob($user->id, Task::class, $task->id, $description));

        return $task;
    }

    public function update($task_id, $payload): bool
    {
        $user = auth()->user();

        $task = Task::find($task_id);
        $task->update($payload);

        $description = $user->name . " update " . $payload['name'] . " task.";
        dispatch(new ProcessLogJob($user->id, Project::class, $task->project_id, $description));
        dispatch(new ProcessLogJob($user->id, Task::class, $task_id, $description));

        return true;
    }

    public function delete($task_id): bool
    {
        $user = auth()->user();

        $task = Task::find($task_id);

        $description = $user->name . " delete " . $task->name . " task from project.";
        dispatch(new ProcessLogJob($user->id, Project::class, $task->project_id, $description));
        dispatch(new ProcessLogJob($user->id, Task::class, $task_id, $description));

        $task->delete();
        return true;
    }

    public function findById(int $task_id)
    {
        $task = Task::where('id', $task_id)->with('notes', 'users')->first();
        return new TaskResource($task);
    }

    public function changeStatus($task_id, $new_status): bool
    {
        $user = auth()->user();

        $task = Task::find($task_id);
        $task->update(['status' => $new_status]);

        $description = $user->name . " change " . $task->name . " task status.";
        dispatch(new ProcessLogJob($user->id, Project::class, $task->project_id, $description));
        dispatch(new ProcessLogJob($user->id, Task::class, $task_id, $description));

        return true;
    }

    public function joinTask($task_id): bool
    {
        $user = User::find(auth()->user()->id);

        $task = Task::find($task_id);

        $already_joined = TaskService::checkIfUserJoined($task_id, $user->id);
        if ($already_joined) {
            return false;
        }

        $user->tasks()->attach($task_id);

        $description = $user->name . " joined " . $task->name . " task.";
        dispatch(new ProcessLogJob($user->id, Project::class, $task->project_id, $description));
        dispatch(new ProcessLogJob($user->id, Task::class, $task_id, $description));

        return true;
    }

    public function leaveTask($task_id): bool
    {
        $user = User::fide(auth()->user()->id);

        $user->tasks()->detach($task_id);

        $task = Task::find($task_id);

        $description = $user->name . " leave " . $task->name . " task.";
        dispatch(new ProcessLogJob($user->id, Project::class, $task->project_id, $description));
        dispatch(new ProcessLogJob($user->id, Task::class, $task_id, $description));

        return true;
    }

    public function assignmentMember($task_id, $user_id): bool
    {
        $user = User::fide(auth()->user()->id);

        $assignment_user = User::find($user_id);

        $task = Task::find($task_id);

        $already_joined = TaskService::checkIfUserJoined($task_id, $assignment_user->id);
        if ($already_joined) {
            return false;
        }

        $assignment_user->tasks()->attach($task_id);

        $description = $user->name . " assignment " . $assignment_user->name . " on " . $task->name . " task.";
        dispatch(new ProcessLogJob($user->id, Project::class, $task->project_id, $description));
        dispatch(new ProcessLogJob($user->id, Task::class, $task_id, $description));

        return true;
    }

    public function all($search, $per_page)
    {
    }
}
