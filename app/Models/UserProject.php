<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserProject extends Model
{
    use HasFactory;

    protected $table = 'user_projects';
    
    protected $fillable = [
        'user_id',
        'project_id',
        'role',
    ];

    protected $hidden = [
    ];

    protected $casts = [
    ];

    // Relationships Methods
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
