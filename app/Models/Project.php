<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $table = 'projects';
    
    protected $fillable = [
        'title',
        'description',
        'status',
        'background_image',
    ];

    protected $hidden = [
    ];

    protected $casts = [
    ];

    // Relationships Methods
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_projects')->withTimestamps();
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function changes()
    {
        return $this->morphMany(ChangesLog::class, 'model');
    }
}
