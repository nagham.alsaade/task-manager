<?php

namespace App\Http\Middleware;

use App\Enums\RoleTypeEnum;
use App\Models\Project;
use App\Models\Task;
use App\Services\ProjectService;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckIsJoinedProject
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, string ...$guards): Response
    {
        $user = auth()->user();

        $is_joined = ProjectService::checkIfUserJoined($request->project_id, $user->id);
        
        if (!$is_joined) {

            $response = [
                'success' => false,
                'data' => [],
                'message' => (__('errors.user_not_joined')),
            ];

            return response()->json($response, 403);
        }
        return $next($request);
    }
}
