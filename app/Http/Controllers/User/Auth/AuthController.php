<?php

namespace App\Http\Controllers\User\Auth;

use App\Contracts\AuthInterface;

use App\Enums\ResponseEnum;

use App\Http\Controllers\Controller;

use App\Http\Resources\User\UserResource;

use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\ConfirmCodeRequest;
use App\Http\Requests\Auth\ForgetPasswordRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\ResendCodeRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct(private AuthInterface $authInterface)
    {
    }

    public function register(RegisterRequest $request)
    {
        try {
            $success = $this->authInterface->register($request->name, $request->email, $request->password);
        } catch (Exception $e) {
            return $this->sendError(__('auth.error_send_code'), 500);
        }

        return $this->sendResponse(__('auth.verify_code'), $success);
    }

    public function confirmAccount(ConfirmCodeRequest $request)
    {
        $success = $this->authInterface->confirmAccount($request->email, 'user-api', $request->code);

        if (!$success) {
            return $this->sendError(__('auth.error_code'));
        }

        return $this->sendResponse(__('auth.register_success'), $success);
    }

    public function login(LoginRequest $request)
    {
        $success = $this->authInterface->login($request->email, $request->password, 'user-api');

        if (!$success) {
            return $this->sendError(__('auth.wrong_credentials'));
        }

        return $this->sendResponse(__('auth.login_success'), $success);
    }

    public function logout(Request $request)
    {
        $success = $this->authInterface->logout($request);
        return $this->sendResponse(__('auth.logout_success'), $success);
    }

    public function forgetPassword(ForgetPasswordRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        try {
            $success = $this->authInterface->forgetPassword($user);
        } catch (Exception $e) {
            return $this->sendError(__('auth.error_send_code'), 500);
        }

        return $this->sendResponse(__('auth.verify_code'), $success);
    }

    public function resendCode(ResendCodeRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        try {
            $success = $this->authInterface->resendCode($user);
        } catch (Exception $e) {
            return $this->sendError(__('auth.error_send_code'), 500);
        }

        return $this->sendResponse(__('auth.code_sent_success'), $success);
    }

    public function confirmCode(ConfirmCodeRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        $success = $this->authInterface->confirmCode($user, $request->code);

        if (!$success) {
            return $this->sendError(__('auth.error_code'));
        }

        $user->is_verified = true;
        $user->save();

        return $this->sendResponse(__('auth.success_verified'), $success);
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        $success = $this->authInterface->resetPassword($user, $request->new_password);
        return $this->sendResponse(__('auth.reset_password_success'), $success);
    }

    public function showProfile()
    {
        $success = new UserResource(auth()->user());
        return $this->sendResponse(ResponseEnum::GET, $success);
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = auth()->user();
        $success = $this->authInterface->changePassword($user, $request->old_password, $request->new_password);

        if (!$success) {
            return $this->sendError(__('auth.wrong_old_password'));
        }

        return $this->sendResponse(__('auth.change_password_success'), $success);
    }
}
