<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static NotStarted()
 * @method static static InProgress()
 * @method static static OnHold()
 * @method static static Completed()
 * @method static static Cancelled()
 * @method static static Deferred()
 * @method static static NeedsReview()
 * @method static static Blocked()
 * @method static static Reopened()
 * @method static static Archived()
 */
final class TaskStatusEnum extends Enum
{
    const NotStarted = 0;
    const InProgress = 1;
    const OnHold = 2;
    const Completed = 3;
    const Cancelled = 4;
    const Deferred = 5;
    const NeedsReview = 6;
    const Blocked = 7;
    const Reopened = 8;
    const Archived = 9;

    public static function isValidValue($value)
    {
        return in_array($value, self::getValues());
    }
}
