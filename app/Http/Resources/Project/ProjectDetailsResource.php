<?php

namespace App\Http\Resources\Project;

use App\Http\Resources\Note\NoteResource;
use App\Http\Resources\Task\TaskResource;
use App\Http\Resources\User\UserProjectResource;
use App\Http\Resources\User\UserResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request)
    {
        return [
            'id' =>  $this->resource->id,
            'title' =>  $this->resource->title,
            'description' =>  $this->resource->description,
            'status' =>  $this->resource->status,
            'background_image' => $this->resource->background_image,
            'created_at' => Carbon::parse($this->resource->created_at)->format('M d, Y h:i A'),
            'users' =>  UserResource::Collection($this->resource->users),
            'tasks' =>  TaskResource::Collection($this->resource->tasks),
        ];
    }
}
