<?php

namespace App\Http\Controllers\User;

use App\Contracts\ProjectInterface;
use App\Enums\ResponseEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Project\AddMemberRequest;
use App\Http\Requests\Project\AllRequest;
use App\Http\Requests\Project\ChangeMemberRoleRequest;
use App\Http\Requests\Project\ChangeStatusRequest;
use App\Http\Requests\Project\CreateRequest;
use App\Http\Requests\Project\GetRequest;
use App\Http\Requests\Project\UpdateRequest;

class ProjectController extends Controller
{

    public function __construct(private ProjectInterface $projectInterface)
    {
    }

    public function create(CreateRequest $request)
    {
        $request = array_filter($request->validated(), function ($value) {
            return ($value !== null);
        });

        $success = $this->projectInterface->create($request);
        return $this->sendResponse(ResponseEnum::ADD, $success);
    }

    public function all(AllRequest $request)
    {
        $success = $this->projectInterface->all($request->search, $request->per_page);
        return $this->sendResponse(ResponseEnum::GET, $success);
    }

    public function show(GetRequest $request)
    {
        $success = $this->projectInterface->findById($request->project_id);
        return $this->sendResponse(ResponseEnum::GET, $success);
    }

    public function update(UpdateRequest $request)
    {
        $request = array_filter($request->validated(), function ($value) {
            return ($value !== null);
        });

        $success = $this->projectInterface->update($request['project_id'], $request);
        return $this->sendResponse(ResponseEnum::UPDATE, $success);
    }

    public function delete(GetRequest $request)
    {
        $success = $this->projectInterface->delete($request->project_id);
        return $this->sendResponse(ResponseEnum::DELETE, $success);
    }

    public function changeStatus(ChangeStatusRequest $request)
    {
        $success = $this->projectInterface->changeStatus($request->project_id, $request->new_status);
        return $this->sendResponse(ResponseEnum::UPDATE, $success);
    }

    public function addMember(AddMemberRequest $request)
    {
        $success = $this->projectInterface->addMember($request->project_id, $request->user_id);

        if(!$success) {
            return $this->sendError(__('errors.already_join_project'));
        }
        return $this->sendResponse(ResponseEnum::ADD, $success);
    }

    public function changeMemberRole(ChangeMemberRoleRequest $request)
    {
        $success = $this->projectInterface->changeMemberRole($request->project_id, $request->user_id, $request->new_role);
        return $this->sendResponse(ResponseEnum::UPDATE, $success);
    }
}
