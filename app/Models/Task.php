<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $table = 'tasks';

    protected $fillable = [
        'name',
        'description',
        'priority',
        'start_date',
        'end_date',
        'status',
        'project_id'
    ];

    protected $hidden = [
        'updated_at',
    ];

    protected $casts = [
        'start_date' => 'datetime:Y-m-d H:i:s',
        'end_date' => 'datetime:Y-m-d H:i:s',
    ];

    // Relationships Methods
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_tasks')->withTimestamps();
    }

    public function notes()
    {
        return $this->hasMany(Note::class);
    }

    public function changes()
    {
        return $this->morphMany(ChangesLog::class, 'model');
    }
}
