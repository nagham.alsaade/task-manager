<?php

namespace App\Services;

use App\Enums\RoleTypeEnum;
use App\Models\UserProject;
use App\Models\UserTask;

/**
 * Class UserService.
 */
class TaskService
{
    public static function checkIfUserJoined($task_id, $user_id) 
    {
        // Get relation between the user and the task.
        $user_task = UserTask::where([['user_id', $user_id], ['task_id', $task_id]])->first();

        // Check if user joined to task.
        if(isset($user_task)) {
            return true;
        }

        return false;
    }
}
