<?php

namespace App\Http\Middleware;

use App\Enums\RoleTypeEnum;
use App\Models\Task;
use App\Services\ProjectService;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckUserNotViewer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, string ...$guards): Response
    {
        $user = auth()->user();

        if(isset($request->project_id)) {
            $project_id = $request->project_id;
        } else if(isset($request->task_id)) {
            $task = Task::findOrFail($request->task_id);
            $project_id = $task->project_id;
        }

        $user_role = ProjectService::checkUserRole($project_id, $user->id);
        
        if ($user_role == RoleTypeEnum::Viewer) {

            $response = [
                'success' => false,
                'data' => [],
                'message' => (__('errors.user_viewer')),
            ];

            return response()->json($response, 403);
        }
        return $next($request);
    }
}
