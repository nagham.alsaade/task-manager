<?php

namespace App\Http\Controllers\User;

use App\Contracts\NoteInterface;
use App\Enums\ResponseEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Note\CreateRequest;
use App\Http\Requests\Note\GetRequest;
use App\Http\Requests\Note\UpdateRequest;

class NoteController extends Controller
{
    public function __construct(private NoteInterface $noteInterface)
    {
    }

    public function create(CreateRequest $request)
    {
        $request = array_filter($request->validated(), function ($value) {
            return ($value !== null);
        });

        $success = $this->noteInterface->create($request);
        return $this->sendResponse(ResponseEnum::ADD, $success);
    }

    public function update(UpdateRequest $request)
    {
        $request = array_filter($request->validated(), function ($value) {
            return ($value !== null);
        });

        $success = $this->noteInterface->update($request['note_id'], $request);
        return $this->sendResponse(ResponseEnum::UPDATE, $success);
    }

    public function delete(GetRequest $request)
    {
        $success = $this->noteInterface->delete($request->note_id);
        return $this->sendResponse(ResponseEnum::DELETE, $success);
    }
}
