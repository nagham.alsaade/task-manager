<?php

namespace App\Http\Controllers\User;

use App\Contracts\ChangesLogInterface;
use App\Enums\ResponseEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangesLog\GetLogRequest;

class ChangesLogController extends Controller
{
    public function __construct(private ChangesLogInterface $taskInterface)
    {
    }

    public function show(GetLogRequest $request)
    {
        $success = $this->taskInterface->show($request->model_type, $request->model_id, $request->per_pgae);
        return $this->sendResponse(ResponseEnum::GET, $success);
    }
}
