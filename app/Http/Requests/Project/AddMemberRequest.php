<?php

namespace App\Http\Requests\Project;

use App\Http\Requests\BaseRequest;
use App\Rules\ValidateRoleType;

class AddMemberRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'project_id' => 'required|integer|exists:projects,id',
            'user_id' => 'required|integer|exists:users,id',
            'role' => ['nullable', 'integer', new ValidateRoleType()]
        ];
    }
}
