<?php

namespace app\Repositories;

use App\Contracts\ChangesLogInterface;
use App\Http\Resources\Log\ChangesLogCollection;
use App\Models\ChangesLog;
use App\Models\Project;
use App\Models\Task;

class ChangesLogRepository implements ChangesLogInterface
{
    public function show($mode_type, $model_id, $per_page = 8)
    {
        if ($mode_type == 'Project') {
            $model_type = Project::class;
        } else {
            $model_type = Task::class;
        }
        $changes = ChangesLog::where([['model_type', $model_type], ['model_id', $model_id]])
            ->orderBy('created_at', 'DESC');

        return new ChangesLogCollection($changes->paginate($per_page));
    }
}
