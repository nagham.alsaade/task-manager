<?php

namespace App\Services;

use App\Enums\RoleTypeEnum;
use App\Models\UserProject;

/**
 * Class UserService.
 */
class ProjectService
{
    public static function checkIfUserAdministrator($project_id) 
    {
        $user = auth()->user();

        // Get relation between the user and the project.
        $user_project = UserProject::where([['user_id', $user->id], ['project_id',$project_id]])->first();

        // Check if user joined to project and his role is administrator.
        if(isset($user_project) && $user_project->role == RoleTypeEnum::Administrator) {
            return true;
        }

        return false;
    }

    public static function checkIfUserJoined($project_id, $user_id) 
    {
        // Get relation between the user and the project.
        $user_project = UserProject::where([['user_id', $user_id], ['project_id', $project_id]])->first();

        // Check if user joined to project.
        if(isset($user_project)) {
            return true;
        }

        return false;
    }

    public static function checkUserRole($project_id, $user_id) 
    {
        // Get relation between the user and the project.
        $user_project = UserProject::where([['user_id', $user_id], ['project_id', $project_id]])->first();
        return $user_project->role;
    }
}
